<?php

namespace Esc\Shopify\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Cookie;

use Esc\Shopify\SameSiteHack\SameSiteCookie;
use Esc\Shopify\SameSiteHack\SameSiteResponseHeaderBag;
use Jenssegers\Agent\Agent;

class SamesiteCookieMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (!$this->checkSameSiteNoneCompatible()) {
            return $response;
        }

        if (property_exists(Cookie::class, 'sameSite') && defined('Symfony\Component\HttpFoundation\Cookie::SAMESITE_NONE')) {
            // SameSite=None is supported in Symfony, replace the cookies with the new value
            foreach ($response->headers->getCookies() as $cookie) {
                $response->headers->setCookie(
                    new Cookie(
                        $cookie->getName(), $cookie->getValue(), $cookie->getExpiresTime(), $cookie->getPath(),
                        $cookie->getDomain(), true, $cookie->isHttpOnly(), $cookie->isRaw(),
                        'None'
                    )
                );
            }
        } else {
            // SameSite=None is not supported in Symfony (i.e. Laravel <5.5), use the workaround
            foreach ($response->headers->getCookies() as $cookie) {
                header('Set-Cookie: '.$cookie->getName().strstr($cookie, '=').'; SameSite=None; Secure', false, $response->getStatusCode());
                $response->headers->removeCookie($cookie->getName(), $cookie->getPath());
            }
        }

        return $response;
    }

    private function checkSameSiteNoneCompatible()
    {
        $compatible = false;

        $this->agent = new Agent();

        try {
            $browser = $this->getBrowserDetails();
            $platform = $this->getPlatformDetails();

            if ($this->agent->is('Chrome') && $browser['major'] >= 67) {
                $compatible = true;
            }

            if ($this->agent->is('iOS') && $platform['major'] > 12) {
                $compatible = true;
            }

            if ($this->agent->is('OS X') &&
                ($this->agent->is('Safari') && !$this->agent->is('iOS')) &&
                $platform['float'] > 10.14
            ) {
                $compatible = true;
            }

            if ($this->agent->is('UCBrowser') &&
                $browser['float'] > 12.13
            ) {
                $compatible = true;
            }

            return $compatible;
        } catch (\Exception $e) {
            return false;
        }
    }

    private function getBrowserDetails()
    {
        $version = $this->agent->version($this->agent->browser());
        $pieces = explode('.', str_replace('_', '.', $version));

        return [
            'major' => $pieces[0],
            'minor' => isset($pieces[1]) ? $pieces[1] : "",
            'float' => sprintf('%s.%s', $pieces[0], isset($pieces[1]) ? $pieces[1] : ""),
        ];
    }

    private function getPlatformDetails()
    {
        $version = $this->agent->version($this->agent->platform());
        $pieces = explode('.', str_replace('_', '.', $version));

        return [
            'major' => $pieces[0],
            'minor' => isset($pieces[1]) ? $pieces[1] : "",
            'float' => sprintf('%s.%s', $pieces[0], isset($pieces[1]) ? $pieces[1] : ""),
        ];
    }
}
