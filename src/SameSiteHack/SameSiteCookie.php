<?php
namespace Esc\Shopify\SameSiteHack;

/**
 * This class patches in support for SameSite=None in Laravel pre-5.5
 * For newer versions where SameSite=None is supported, this isn't used.
 */


class SameSiteCookie {
    public $__cookie;

    public function __construct($cookie) {
        $this->__cookie = $cookie;
    }

    public function __call($method, $args) {
        return call_user_func_array([$this->__cookie, $method], $args);
    }
    public function __get($key) {
        return $this->__cookie[$key];
    }
    public function __set($key, $value){
        return $this->__cookie[$key] = $value;
    }
    public function __isset($key) {
        return isset($this->__cookie[$key]);
    }
    public function __toString() {
        $r = $this->__cookie->__toString();
        // Append SameSite=None if it is not included
        if (strpos(strtolower($r), 'samesite=none') === false) {
            return $r . '; SameSite=None';
        }
        return $r;
    }
}
