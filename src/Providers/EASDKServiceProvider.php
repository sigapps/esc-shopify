<?php
namespace Esc\Shopify\Providers;

use Illuminate\Support\ServiceProvider;

use Esc\Shopify\EASDK;


class EASDKServiceProvider extends ServiceProvider {
    public function register() {
        $this->publishes([
            __DIR__.'/../database/' => database_path('migrations')
        ]);

        $this->app->bind('ShopifyEASDK', function($app) {
            return new EASDK;
        });

        $this->app->bind(Esc\Shopify\EASDK::class, function($app) {
            return new EASDK;
        });

    }

    public function boot() {

    }
}
