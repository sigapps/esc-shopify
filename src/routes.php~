<?php

Route::get('oauth', function(\Illuminate\Http\Request $request) {
    $shopDomain = $request->get('shop');

    if (!$shopDomain) {
        return abort(403);
    } else {
        \Log::info('esc/shopify: Got OAuth request for ['.$shopDomain.']');

        $api = app('ShopifyAPI');
        $api->setup([
            'SHOP_DOMAIN' => $shopDomain
        ]);
        $easdk = app('ShopifyEASDK');
        $easdk->setAPI($api);

        echo $easdk->hostedRedirect($api->installURL(\URL::to('/oauth/done/'), \Config::get('esc_shopify.scopes')));
        exit;
    }
});

Route::get('oauth/done', function(\Illuminate\Http\Request $request) {
    $shopDomain = $request->get('shop');
    $api = app('ShopifyAPI');
    $api->setup([
        'SHOP_DOMAIN' => $shopDomain
    ]);

    if (!$api->verifyRequest($request->all())) {
        \Log::info('esc/shopify: OAuth request for ['.$shopDomain.'] could not be verified.');
        return redirect()->to('/oauth?shop='.$shopDomain);
    }

    try {
        $accessToken = $api->getAccessToken($request->get('code'));
        $api->setup([
            'ACCESS_TOKEN' => $accessToken
        ]);
    } catch (\Exception $ex) {
        \Log::info('esc/shopify: OAuth request for ['.$shopDomain.'] failed - retrying.');
        return redirect()->to('/oauth/?shop='.$shopDomain);
    }

    $shop = \App\Shop::findByDomain($shopDomain);
    if ($shop) {
        \Log::info('esc/shopify: OAuth request for ['.$shopDomain.'] successful - logging in.');
        $shop->access_token = $accessToken;
        $shop->save();

        $shop->login();

        return redirect()->to('/app');
    } else {
        \Log::info('esc/shopify: OAuth request for ['.$shopDomain.'] successful - creating account.');
        $user = new \App\User;
        $user->email = 'owner@'.$shopDomain;
        $user->save();

        $shop = new \App\Shop;
        $shop->shop_domain = $shopDomain;
        $shop->user_id = $user->id;
        $shop->access_token = $accessToken;
        $shop->save();

        $shop->login();

        if (count(\Config::get('esc_shopify.webhooks')) > 0) {
            \Log::info('esc/shopify: OAuth request for ['.$shopDomain.'] successful - setting up webhooks.');
            foreach (\Config::get('esc_shopify.webhooks') as $hook) {
                if (count($api->call('get', '/admin/webhooks.json', ['topic' => $hook['topic'], 'address' => $hook['address']])->webhooks) == 0) {
                    $api->call('post', '/admin/webhooks.json', [
                        'webhook' => $hook
                    ]);
                }
            }
        }

        if (count(\Config::get('esc_shopify.script_tags')) > 0) {
            \Log::info('esc/shopify: OAuth request for ['.$shopDomain.'] successful - setting up scripttags.');

            foreach (\Config::get('esc_shopify.script_tags') as $url) {
                if (count($api->call('get', '/admin/script_tags.json', ['src' => $url])->script_tags) == 0) {
                    $api->call('post', '/admin/script_tags.json', [
                        'script_tag' => [
                            'event' => 'onload',
                            'src' => $url
                        ]
                    ]);
                }
            }
        }

        return redirect()->to('/app');
    }
});
